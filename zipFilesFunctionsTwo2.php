<?php
    //we need session.
    // session_start();
    // $_SESSION["zipName"] = $zipName;
    // echo $_SESSION["path"];

    //Creates a compressed zip file.
    //https://davidwalsh.name/create-zip-php

    //$overwrite = true/false;
    function create_zip($files = array(), $destination = '', $overwrite = false) {
        //if the zip file already exists and overwrite is false, return false
        if(file_exists($destination) && !$overwrite) {
            echo 'Zip/Rar aready exist, sorry! Overwrite is not possible. <br>';
            return false;
        }

        //validate files this way or -> //$valid_files = array_filter($files, 'file_exists');
        $valid_files = array();
        //if files were passed in...
        if(is_array($files)) {
            //cycle through each file
            foreach($files as $file) {
                //make sure the file exists
                if(file_exists($file)) {
                    $valid_files[] = $file;
                }
            }
        }

        //if we have good files...
        if(count($valid_files)) {
            //create the archive
            $zip = new ZipArchive();
            if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
                echo "Files are valid.";
                return false;
            }

            //add the files
            foreach($valid_files as $file) {
                $zip->addFile($file, $file);
                // $zip->addFile($file,basename($file));// if you add files in different location.
            }

            //debug
            echo 'The zip archive contains:<b> ', $zip->numFiles,'</b> files with a status of: ', $zip->status.".<br>"; // [status] => 0 [statusSys] => 0 [numFiles] => 3 [filename]
            echo 'Directory of your zip/rar:<b> ', $zip->filename,'</b> with a status of: ', $zip->statusSys.".<br>"; // [status] => 0 [statusSys] => 0 [numFiles] => 3 [filename]

            // $first_value = reset($array); // First Element's Value
            // $first_key = key($array); // First Element's Key
            // reset arrays pointers to the first element.
            // reset($zip);

            //close the zip -- done!
            $zip->close();

            //check to make sure the file exists
            return file_exists($destination);

            if (file_exists($destination) == true) {
                echo "Archieve with that name already exists. <br>";
            }
        }
        else
        {
            return false;
        }
    }

    //array with files to be archieved.
    $files_to_zip = array(
        'test/test_file_1.html',
        'test/test_file_2.html',
        'test/test_file_3.html',
        'test/test_file_4.html',
        'test/test_file_5.html',
        'test/test_file_6.html'
    );
    //if true, good; if false, zip creation failed.

    $zipNameExtension = "rar"; // rar or zip.
    // $zipNameExtension = "zip"; // rar or zip
    $zipName = "my_archive_okayy"; //name of the archieve.
    $zipNameFinal = $zipName.".".$zipNameExtension;
    $result = create_zip($files_to_zip, $zipNameFinal);

    // $zipName = array_shift(array_slice($files_to_zip, 0, 1));

    //session variables.
    // $_SESSION["zipName"] = $zipNameFinal;
    // echo $zipNameFinal;

    // var_dump($zip);
?>