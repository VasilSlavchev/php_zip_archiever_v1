<?php

    //we need session in order to save our zip/rar file name for extracting.
    session_start();
    //take the name of archieve from session.
    $zipName = $_SESSION["zipName"];

    //debug
    echo 'Archieve name: <b>'.$zipName."</b><br>";

    if ($zipName == '') {
        // echo 'Sorry, no zip/rar is selected! ';
        // echo '['.$zipName."] <br>";
        // exit;
    }
    else {
        //we take zip name from session.
        $zipName = $_SESSION["zipName"];
    }
  
    //folder where files would be extracted.
    $zipExtractPath = "extracted"; 

    $zip = new ZipArchive;
    if ($zip->open($zipName) === true) {

        for($i = 0; $i < $zip->numFiles; $i++) {

            $zip->extractTo($zipExtractPath, array($zip->getNameIndex($i)));

            // here you can run a custom function for the particular extracted file.
            echo 'File index: '.$i.' in: '.$zip->filename.' -> successeful extracted! <br>';
            
        }

        $zip->close();

    }

//
?>